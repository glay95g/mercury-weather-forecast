import React, { useEffect, useState } from 'react';
import './App.scss';
import ForecastCard from './components/view/ForecastCard';
import ICity from './models/ICity';
import IDayForecast from './models/IDayForecast';
import DateHelper from './services/DateHelper';
import WeatherApiService from './services/WeatherApiService';
import cities from './constants';

function App(): JSX.Element {
  const [pastDayForcastSelectedCity, setPastDayForcastSelectedCity] = useState<ICity | null>(null);
  const [pastDayForcastSelectedDate, setPastDayForcastSelectedDate] = useState<number | null>(null);
  const [pastDayForecastData, setPastDayForecastData] = useState<IDayForecast | null>(null);
  const [isPastDayForecastData, setIsPastDayForecastDataLoading] = useState<boolean>(false);

  const [sevenDaysForcastSelectedCity,
    setSevenDaysForcastSelectedCity] = useState<ICity | null>(null);
  const [sevenDaysForecastData,
    setSevenDaysForecastData] = useState<Array<IDayForecast> | null>(null);
  const [isSevenDaysForecastDataLoading,
    setIsSevenDaysForecastDataLoading] = useState<boolean>(false);

  useEffect(() => {
    if (!!pastDayForcastSelectedCity && !!pastDayForcastSelectedDate) {
      setIsPastDayForecastDataLoading(true);
      WeatherApiService.getPastDayForecast(pastDayForcastSelectedCity,
        pastDayForcastSelectedDate, (data: IDayForecast | null) => {
          setPastDayForecastData(data);
          setIsPastDayForecastDataLoading(false);
        });
    }
  }, [pastDayForcastSelectedCity, pastDayForcastSelectedDate]);

  useEffect(() => {
    if (sevenDaysForcastSelectedCity) {
      setIsSevenDaysForecastDataLoading(true);
      WeatherApiService.getSevenDaysForecast(sevenDaysForcastSelectedCity,
        (data: IDayForecast[] | null) => {
          setSevenDaysForecastData(data);
          setIsSevenDaysForecastDataLoading(false);
        });
    }
  }, [sevenDaysForcastSelectedCity]);

  const changeSelectedCity = (city: ICity, isSingleDateForecast?: boolean) => {
    if (isSingleDateForecast) {
      setPastDayForcastSelectedCity(city);
    } else {
      setSevenDaysForcastSelectedCity(city);
    }
  };

  const changeSelectedDate = (selectedDate: Date) => {
    const selectedDateTimeUTC = DateHelper.convertDateTimeToUTCTime(selectedDate.getTime()
       + Math.round(DateHelper.oneDayOffset / 2));
    setPastDayForcastSelectedDate(selectedDateTimeUTC);
  };

  return (
    <div className="app">
      <header className="app__header">
        <p className="app__title">
          <span>Weather</span>
          <span>forecast</span>
        </p>
      </header>

      <main className="app__main">
        <div className="app__forecast-column">
          <ForecastCard
            isSingleDateForecast={false}
            cities={cities}
            forecastData={sevenDaysForecastData}
            isLoading={isSevenDaysForecastDataLoading}
            changeSelectedCity={changeSelectedCity}
            changeSelectedDate={changeSelectedDate}
          />
        </div>
        <div className="app__forecast-column">
          <ForecastCard
            isSingleDateForecast
            cities={cities}
            forecastData={pastDayForecastData}
            isLoading={isPastDayForecastData}
            changeSelectedCity={changeSelectedCity}
            changeSelectedDate={changeSelectedDate}
          />
        </div>
      </main>

      <footer className="app__footer">
        C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT
      </footer>
    </div>
  );
}

export default App;
