import React, { ChangeEvent, useState } from 'react';
import { DateSystemViewFormat } from '../../services/DateHelper';
import './BaseDateInput.scss';

interface BaseDateInputProps extends React.InputHTMLAttributes<HTMLInputElement> {
  placeholder: string,
  minDate: DateSystemViewFormat,
  maxDate: DateSystemViewFormat,
  onChangeDate: (date: Date) => void
}

function BaseDateInput(props: BaseDateInputProps): JSX.Element {
  const {
    placeholder, minDate, maxDate, onChangeDate,
  } = props;

  const [isActive, setIsActive] = useState(false);

  const dateChanged = (dateChangedEvent: ChangeEvent<HTMLInputElement>) => {
    const selectedDate = new Date(dateChangedEvent.target.value);
    onChangeDate(selectedDate);
  };

  return (
    <div className={`date-input-component ${isActive ? 'date-input-component_active' : ''}`} onBlur={() => setIsActive(false)}>
      <input
        type="date"
        min={minDate}
        max={maxDate}
        placeholder={placeholder}
        className="date-input-component__input"
        onClick={() => setIsActive(true)}
        onChange={dateChanged}
      />
    </div>
  );
}

export default BaseDateInput;
