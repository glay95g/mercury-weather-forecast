import React, { ChangeEvent, useState } from 'react';
import IBaseObject from '../../models/IBaseObject';
import './BaseSelect.scss';

interface BaseSelectProps extends React.InputHTMLAttributes<HTMLInputElement> {
  placeholder: string,
  options: IBaseObject[],
  onChangeOption: (option: IBaseObject) => void
}

function BaseSelect(props: BaseSelectProps): JSX.Element {
  const { placeholder, options, onChangeOption } = props;

  const [isActive, setIsActive] = useState(false);
  const [selectedValue, setSelectedValue] = useState('');

  const optionsListView = options?.map(
    (option: IBaseObject) => <option value={option.id} key={option.id}>{option.name}</option>,
  );

  const optionChanged = (optionChangedEvent: ChangeEvent<HTMLSelectElement>) => {
    const foundOption: IBaseObject | undefined = options.find(
      (o: IBaseObject) => o.id.toString() === optionChangedEvent.target.value.toString(),
    );
    setSelectedValue(optionChangedEvent.target.value.toString());
    if (foundOption) {
      onChangeOption(foundOption);
    }
  };

  return (
    <div className={`select-component ${isActive ? 'select-component_active' : ''}`} onBlur={() => setIsActive(false)}>
      <select
        className={`select-component__select ${selectedValue === '' ? 'select-component__select_empty' : ''}`}
        onFocus={() => setIsActive(true)}
        onChange={optionChanged}
      >
        <option value="" hidden style={{ color: 'red' }}>{placeholder}</option>
        {optionsListView}
      </select>
    </div>
  );
}

export default BaseSelect;
