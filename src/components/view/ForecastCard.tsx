/* eslint-disable jsx-a11y/control-has-associated-label */
import React, { useRef, useState } from 'react';
import './ForecastCard.scss';
import './CardsCarousel.scss';
import forecastPlaceholderImage from '../../assets/placeholder/forecast-placeholder.svg';
import BaseSelect from '../shared/BaseSelect';
import BaseDateInput from '../shared/BaseDateInput';
import WeatherCard from './WeatherCard';
import DateHelper, { DateSystemViewFormat } from '../../services/DateHelper';
import ICity from '../../models/ICity';
import IDayForecast from '../../models/IDayForecast';
import IBaseObject from '../../models/IBaseObject';

interface ForecastCardProps extends React.InputHTMLAttributes<HTMLInputElement> {
  isSingleDateForecast: boolean,
  cities: ICity[],
  forecastData: IDayForecast | IDayForecast[] | null,
  isLoading: boolean,
  changeSelectedCity: (cityOption: ICity, isSingleDateForecast?: boolean) => void,
  changeSelectedDate: (date: Date) => void
}

function ForecastCard(props: ForecastCardProps): JSX.Element {
  const {
    isSingleDateForecast, cities, forecastData, isLoading, changeSelectedCity, changeSelectedDate,
  } = props;

  const [isScrolledToStart, setIsScrolledToStart] = useState<boolean>(true);
  const [isScrolledToEnd, setIsScrolledToEnd] = useState<boolean>(false);

  const todayDate: Date = new Date();
  todayDate.setTime(todayDate.getTime() - DateHelper.oneDayOffset);
  const yesterdayDateString: DateSystemViewFormat = DateHelper.getDateString(todayDate);
  const fiveDaysAgoDate: Date = new Date();
  fiveDaysAgoDate.setTime(fiveDaysAgoDate.getTime() - DateHelper.oneDayOffset * 5);
  const fiveDaysAgoDateString: DateSystemViewFormat = DateHelper.getDateString(fiveDaysAgoDate);

  const weatherCardsSlides = useRef(null);
  const weatherCardWidth = 184;

  const slidesScrolled = (scrollEvent: React.UIEvent<HTMLDivElement>) => {
    const scrollEventTarget: HTMLDivElement = (scrollEvent.target as HTMLDivElement);
    if (scrollEventTarget.scrollLeft === 0) {
      setIsScrolledToStart(true);
      setIsScrolledToEnd(false);
    } else if (scrollEventTarget.scrollLeft
      === (scrollEventTarget.scrollWidth - scrollEventTarget.offsetWidth)) {
      setIsScrolledToStart(false);
      setIsScrolledToEnd(true);
    } else {
      setIsScrolledToStart(false);
      setIsScrolledToEnd(false);
    }
  };

  const scrollWeatherCardsSlidesToRightSide = () => {
    if (!!weatherCardsSlides && !!weatherCardsSlides.current) {
      const slides = (weatherCardsSlides.current as unknown as Element);
      slides.scroll(slides.scrollLeft + weatherCardWidth, 0);
    }
  };
  const scrollWeatherCardsSlidesToLeftSide = () => {
    if (!!weatherCardsSlides && !!weatherCardsSlides.current) {
      const slides = (weatherCardsSlides.current as unknown as Element);
      slides.scroll(slides.scrollLeft - weatherCardWidth, 0);
    }
  };
  let forecastView;
  if (isSingleDateForecast) {
    forecastView = forecastData ? (
      <div style={{ margin: '10px 0', width: '100%' }}>
        <WeatherCard
          isAdaptiveWidth
          dayForecast={forecastData as IDayForecast}
          isLoading={isLoading}
        />
      </div>
    ) : null;
  } else {
    forecastView = forecastData ? (
      <div className="cards-carousel">
        <div className="cards-carousel__slides" ref={weatherCardsSlides} onScroll={slidesScrolled}>
          {forecastData && (forecastData as IDayForecast[]).map((f: IDayForecast) => (
            <div key={f.date}>
              <WeatherCard
                isAdaptiveWidth={false}
                dayForecast={f}
                isLoading={isLoading}
              />
            </div>
          ))}
        </div>
        <button
          type="button"
          className={`cards-carousel__switch-button cards-carousel__switch-button_left 
            ${isScrolledToStart ? 'cards-carousel__switch-button_disabled' : ''}`}
          onClick={scrollWeatherCardsSlidesToLeftSide}
        />
        <button
          type="button"
          className={`cards-carousel__switch-button cards-carousel__switch-button_right 
            ${isScrolledToEnd ? 'cards-carousel__switch-button_disabled' : ''}`}
          onClick={scrollWeatherCardsSlidesToRightSide}
        />
      </div>
    )
      : null;
  }

  const placeholderView = (!forecastData && (
  <div className="forecast-card__placeholder">
    <img src={forecastPlaceholderImage} alt="Cloud" className="forecast-card__placeholder-image" />
    <span className="forecast-card__placeholder-title">
      Fill in all the fields and the weather will be displayed
    </span>
  </div>
  ));

  return (
    <div className="forecast-card">
      <div className="forecast-card__header">
        <h1 className="forecast-card__title">{isSingleDateForecast ? 'Forecast for a Date in the Past' : '7 Days Forecast'}</h1>
      </div>
      <div className="forecast-card__search-bar">
        <BaseSelect
          placeholder="Select city"
          options={cities}
          onChangeOption={
              (city: IBaseObject) => changeSelectedCity(city as ICity, isSingleDateForecast)
          }
        />
        {isSingleDateForecast && (
        <BaseDateInput
          placeholder="Select date"
          minDate={fiveDaysAgoDateString}
          maxDate={yesterdayDateString}
          onChangeDate={changeSelectedDate}
        />
        )}
      </div>
      <div className="forecast-card__information">
        {placeholderView}
        {forecastView}
        <div />
      </div>
    </div>
  );
}

export default React.memo(ForecastCard);
