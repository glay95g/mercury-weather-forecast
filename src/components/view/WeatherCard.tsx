import React from 'react';
import IDayForecast from '../../models/IDayForecast';
import WeatherApiService from '../../services/WeatherApiService';
import './WeatherCard.scss';

interface WeatherCardProps extends React.InputHTMLAttributes<HTMLInputElement> {
  isAdaptiveWidth: boolean,
  isLoading: boolean,
  dayForecast: IDayForecast,
}

function WeatherCard(props: WeatherCardProps): JSX.Element {
  const { isAdaptiveWidth, isLoading, dayForecast } = props;

  const dayForecastTemperatureView = dayForecast ? (`${(dayForecast.temperature > 0 ? '+' : '') + dayForecast.temperature}°`) : '';

  const weatherCardAdaptiveWidthClass = `${isAdaptiveWidth ? 'weather-card_adaptive-width' : ''}`;
  const weatherCardALoadingClass = `${isLoading ? 'weather-card_loading' : ''}`;

  return (
    <div className={`weather-card ${weatherCardAdaptiveWidthClass} ${weatherCardALoadingClass}`}>
      <div className="weather-card__top">
        <p className="weather-card__date">{dayForecast?.date ?? ''}</p>
      </div>
      <div className="weather-card__middle">
        {!!dayForecast && (
        <img
          src={WeatherApiService.getWeatherIconURL(dayForecast.weather.iconId)}
          alt={dayForecast.weather.title}
          title={dayForecast.weather.title}
          className="weather-card__weather-icon"
        />
        )}
      </div>
      <div className="weather-card__bottom">
        <p className="weather-card__temperature">{dayForecastTemperatureView}</p>
      </div>
    </div>
  );
}

export default React.memo(WeatherCard);
