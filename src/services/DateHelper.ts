export type DateUserViewFormat = `${number} ${string} ${number}`;
export type DateSystemViewFormat = `${string}-${string}-${string}`;

const DateHelper = {
  oneDayOffset: 24 * 60 * 60 * 1000,
  getDateString(dateToParse: Date): DateSystemViewFormat {
    return dateToParse.toISOString().substring(0, 10) as DateSystemViewFormat;
  },
  convertDateTimeToUTCTime(dateTime: number): number {
    return Math.floor(dateTime / 1000);
  },
  convertUTCTimeToDate(utcTime: number): Date {
    return new Date(utcTime * 1000);
  },
  convertDateToUserView(dateToConvert: Date): DateUserViewFormat {
    return <const>`${dateToConvert.getDate()} ${
      dateToConvert.toLocaleString('en-GB', { month: 'short' })} ${dateToConvert.getFullYear()}`;
  },
};

export default DateHelper;
