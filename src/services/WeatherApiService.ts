import ICity from '../models/ICity';
import IDayForecast from '../models/IDayForecast';
import DateHelper from './DateHelper';
import { isErrorResponse } from './models/ErrorResponse';
import { PastDayForecastResponse } from './models/PastDayForecastResponse';
import { SevenDaysForecastResponse } from './models/SevenDaysForecastResponse';

function parseObjectToQueryString(obj: Record<string, string>) {
  return new URLSearchParams(obj).toString();
}

const weatherApiKey = 'dd0c5383ed3ef5146baae1a53c5225f6';
const weatherIconBaseURL = 'http://openweathermap.org/img/wn/';

const WeatherApiService = {
  getWeatherIconURL(iconId: string): string {
    return `${weatherIconBaseURL}${iconId}@2x.png`;
  },
  getPastDayForecast(
    city: ICity,
    dateTime: number,
    callback: (pastDayForecast: IDayForecast | null) => void,
  ): void {
    const pastDayForecastRequestParameters = {
      lat: String(city.latitude),
      lon: String(city.longitude),
      appid: weatherApiKey,
      dt: String(dateTime),
      units: 'metric',
    };
    fetch(
      `https://api.openweathermap.org/data/2.5/onecall/timemachine?${parseObjectToQueryString(
        pastDayForecastRequestParameters,
      )}`,
    )
      .then((res) => res.json())
      .then((pastDayForecastData: PastDayForecastResponse) => {
        if (isErrorResponse(pastDayForecastData)) {
          callback(null);
          throw new Error(pastDayForecastData.message);
        }
        const pastDayForecastDataCurrentForecast = pastDayForecastData.current;
        const pastDayForecast: IDayForecast = {
          date: DateHelper.convertDateToUserView(
            DateHelper.convertUTCTimeToDate(pastDayForecastDataCurrentForecast.dt),
          ),
          temperature: Math.round(pastDayForecastDataCurrentForecast.temp),
          weather: {
            title: pastDayForecastDataCurrentForecast.weather[0].main,
            iconId: pastDayForecastDataCurrentForecast.weather[0].icon,
          },
        };
        callback(pastDayForecast);
      });
  },
  getSevenDaysForecast(city: ICity,
    callback: (sevenDaysForecast: IDayForecast[] | null) => void): void {
    const sevenDaysForecastRequestParameters = {
      lat: String(city.latitude),
      lon: String(city.longitude),
      appid: weatherApiKey,
      units: 'metric',
    };
    fetch(
      `https://api.openweathermap.org/data/2.5/onecall?${parseObjectToQueryString(
        sevenDaysForecastRequestParameters,
      )}`,
    )
      .then((res) => res.json())
      .then((sevenDaysForecastData: SevenDaysForecastResponse) => {
        if (isErrorResponse(sevenDaysForecastData)) {
          callback(null);
          throw new Error(sevenDaysForecastData.message);
        }
        const sevenDaysForecast: IDayForecast[] = sevenDaysForecastData.daily
          .slice(1).map((oneDayForecast) => ({
            date: DateHelper.convertDateToUserView(
              DateHelper.convertUTCTimeToDate(oneDayForecast.dt),
            ),
            temperature: Math.round(oneDayForecast.temp.day),
            weather: {
              title: oneDayForecast.weather[0].main,
              iconId: oneDayForecast.weather[0].icon,
            },
          }));
        callback(sevenDaysForecast);
      });
  },
};

export default WeatherApiService;
