/* eslint-disable @typescript-eslint/ban-types */
export interface ErrorResponse {
  cod: string | number;
  message: string;
}

export function isErrorResponse(serverResponse: unknown): serverResponse is ErrorResponse {
  return (
    Boolean(serverResponse)
    && typeof serverResponse === 'object'
    && 'cod' in (serverResponse as object)
    && 'message' in (serverResponse as object)
  );
}
