import { WeatherParametersResponse } from './WeatherParametersResponse';

type HourlyPastDayForecastResponse = {
  clouds: number;
  dew_point: number;
  dt: number;
  feels_like: {
    day: number;
    eve: number;
    morn: number;
    night: number;
  };
  humidity: number;
  pressure: number;
  temp: number;
  visibility: number;
  weather: WeatherParametersResponse[];
  wind_deg: number;
  wind_speed: number;
};

type CurrentPastDayForecastResponse = {
  clouds: number;
  dew_point: number;
  dt: number;
  feels_like: {
    day: number;
    eve: number;
    morn: number;
    night: number;
  };
  humidity: number;
  pressure: number;
  sunrise: number;
  sunset: number;
  temp: number;
  visibility: number;
  weather: WeatherParametersResponse[];
  wind_deg: number;
  wind_speed: number;
};

export interface PastDayForecastResponse {
  current: CurrentPastDayForecastResponse;
  hourly: HourlyPastDayForecastResponse[];
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: string;
}
