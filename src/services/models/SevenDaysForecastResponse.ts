import { WeatherParametersResponse } from './WeatherParametersResponse';

type DailyForecastResponse = {
  clouds: number;
  dew_point: number;
  dt: number;
  feels_like: {
    day: number;
    eve: number;
    morn: number;
    night: number;
  };
  humidity: number;
  moon_phase: number;
  moonrise: number;
  moonset: number;
  pop: number;
  pressure: number;
  sunrise: number;
  sunset: number;
  temp: {
    day: number;
    eve: number;
    morn: number;
    night: number;
    max: number;
    min: number;
  };
  uvi: number;
  weather: WeatherParametersResponse[];
  wind_deg: number;
  wind_gust: number;
  wind_speed: number;
};

export interface SevenDaysForecastResponse {
  daily: DailyForecastResponse[];
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: number;
}
