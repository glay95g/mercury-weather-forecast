export interface WeatherParametersResponse {
  description: string;
  icon: string;
  id: number;
  main: string;
}
