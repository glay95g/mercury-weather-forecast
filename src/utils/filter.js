/* eslint-disable @typescript-eslint/no-unused-vars */
// @ts-check

/**
 * @function
 * @template T
 * @param {(element: T) => boolean} predicate
 * @param {T[]} collection
 * @returns {T[]}
 */
function filter(predicate, collection) {
  return collection.reduce(
    (/** @type {T[]} */ previousValue, currentValue) => (predicate(currentValue)
      ? [...previousValue, currentValue]
      : previousValue),
    [],
  );
}
