/* eslint-disable @typescript-eslint/no-unused-vars */
// @ts-check

/**
 * @function
 * @template T
 * @template V
 * @param {(element: T) => V} callback
 * @param {T[]} collection
 * @returns {V[]}
 */
function map(callback, collection) {
  return collection.reduce(
    (/** @type {V[]} */ previousValue, currentValue) => [...previousValue, callback(currentValue)],
    [],
  );
}
