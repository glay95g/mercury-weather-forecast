// eslint-disable-next-line @typescript-eslint/no-unused-vars
function merge<T, V>(firstObject: T, secondObject: V): T & V {
  return { ...firstObject, ...secondObject };
}

export {};
