type Padding = `${number}px`;

interface PaddingValues {
  top: Padding;
  right: Padding;
  bottom: Padding;
  left: Padding;
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function calculatePaddings(): PaddingValues;
function calculatePaddings(padding: number): PaddingValues;
function calculatePaddings(
  paddingHorizontal: number,
  paddingVertical?: number
): PaddingValues;
function calculatePaddings(
  paddingTop: number,
  paddingHorizontal: number,
  paddingBottom: number
): PaddingValues;
function calculatePaddings(
  paddingTop: number,
  paddingRight: number,
  paddingBottom: number,
  paddingLeft: number
): PaddingValues;
function calculatePaddings(
  paddingTop?: number,
  paddingRight?: number,
  paddingBottom?: number,
  paddingLeft?: number,
): PaddingValues {
  return {
    top: <const>`${paddingTop || 0}px`,
    right: <const>`${paddingRight || 0}px`,
    bottom: <const>`${paddingBottom || 0}px`,
    left: <const>`${paddingLeft || 0}px`,
  };
}

export {};
